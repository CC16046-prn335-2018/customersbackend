/* eslint-disable camelcase */
import { Op } from 'sequelize';
import HttpCode from '../../configs/httpCode.mjs';
import DB from '../nucleo/DB.mjs';

import {
  Cases, CombinedCase, CtlStatus, CtlTiers, Teams, Usuario,
} from '../models/index.mjs';

export default class CasesController {
  static async index(req, res) {
    const { id } = req.usuario;
    const closeStatus = await CtlStatus.findOne({ where: { name: 'Closed' } });
    const casesList = await Cases.findAll(
      {
        include: [CtlStatus, Teams, Usuario, CtlTiers],
        where: { user_id: id, current_status_id: { [Op.ne]: closeStatus.id } },
      },

    );
    return res.status(HttpCode.HTTP_OK).json(casesList);
  }

  static async store(req, res) {
    const connection = DB.connection();
    const transact = await connection.transaction();
    const { id } = req.usuario;
    const {
      name,
      track_code,
      description,
      current_status_id,
      team_id,
      tier_id,
    } = req.body;
    try {
      await Cases.create({
        name,
        track_code,
        description,
        current_status_id,
        team_id,
        user_id: id,
        tier_id,
      }, {
        transaction: transact,
      });
      await transact.commit();
      return res.status(HttpCode.HTTP_CREATED).json({
        message: 'Creado satisfactoriamente.',
      });
    } catch (error) {
      await transact.rollback();
      throw error;
    }
  }

  static async closeCase(req, res) {
    const { id_case } = req.body;
    const singleCase = await Cases.findOne({
      where: {
        id: id_case,
      },
    });
    const closeStatus = await CtlStatus.findOne({ where: { name: 'Closed' } });
    await singleCase.update({ current_status_id: closeStatus.id });
    res.status(HttpCode.HTTP_OK).json({
      message: `Case with id: ${id_case} was closed.`,
    });
  }

  static async escaleCase(req, res) {
    const {
      id_tier, id_case, isCustom, statusName, statusDescription,
    } = req.body;
    const dataToUpdate = {};
    const exist = await Cases.findOne({ where: { id: id_case } });
    if (!exist) {
      dataToUpdate.tier_id = id_tier;
      return res.status(HttpCode.HTTP_NOT_FOUND).json({
        message: `Tier with id: ${id_tier} was not found`,
      });
    }
    if (isCustom) {
      const createdStatus = await CtlStatus.create({
        name: statusName,
        description: statusDescription,
      });
      dataToUpdate.current_status_id = createdStatus.id;
    }
    dataToUpdate.tier_id = id_tier;
    dataToUpdate.case_id = id_case;
    await exist.update(dataToUpdate);

    return res.status(HttpCode.HTTP_OK).json({
      message: 'The case has been escaled successfully',
    });
  }

  static async getCasesToCombine(req, res) {
    const {
      current_status_id, team_id, tier_id, id,
    } = req.query;

    const casesList = await Cases.findAll({
      where: {
        current_status_id,
        team_id,
        tier_id,
        id: {
          [Op.ne]: id,
        },
      },
    });

    res.status(HttpCode.HTTP_OK).json({ casesList });
  }

  static async combineCases(req, res) {
    const connection = DB.connection();
    const t = await connection.transaction();
    const { casesToCombine, name } = req.body;

    try {
      const combinedCase = await CombinedCase.create({
        name,
      }, { transaction: t });
      if (combinedCase) {
        await combinedCase.addCases(casesToCombine, { transaction: t });
      }
      await t.commit();
    } catch (error) {
      await t.rollback();
      throw error;
    }
    res.status(HttpCode.HTTP_OK).json({
      message: 'Combined successfully',
    });
  }

  static async getCombinedCases(req, res) {
    const combinedCases = await CombinedCase.findAll({
      include: [Cases],
    });

    res.status(HttpCode.HTTP_OK).json(combinedCases);
  }
}
