import HttpCode from '../../configs/httpCode.mjs';
import DB from '../nucleo/DB.mjs';

import {
  Teams,
} from '../models/index.mjs';

export default class TeamsController {
  static async index(req, res) {
    const teamsList = await Teams.findAll();
    return res.status(HttpCode.HTTP_OK).json(teamsList);
  }

  static async store(req, res) {
    const connection = DB.connection();
    const transact = await connection.transaction();

    const {
      name,
    } = req.body;
    try {
      await Teams.create({
        name,
      }, {
        transaction: transact,
      });
      await transact.commit();
      return res.status(HttpCode.HTTP_CREATED).json({
        message: 'Creado satisfactoriamente.',
      });
    } catch (error) {
      await transact.rollback();
      throw error;
    }
  }
}
