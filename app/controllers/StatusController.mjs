/* eslint-disable camelcase */
import HttpCode from '../../configs/httpCode.mjs';

import {
  CtlStatus,
} from '../models/index.mjs';

export default class StatusController {
  static async index(req, res) {
    const teamsList = await CtlStatus.findAll();
    return res.status(HttpCode.HTTP_OK).json(teamsList);
  }
}
