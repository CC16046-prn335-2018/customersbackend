/* eslint-disable camelcase */
import HttpCode from '../../configs/httpCode.mjs';
import DB from '../nucleo/DB.mjs';

import {
  CtlTiers,
} from '../models/index.mjs';

export default class TiersController {
  static async index(req, res) {
    const ctl_tiersList = await CtlTiers.findAll();
    return res.status(HttpCode.HTTP_OK).json(ctl_tiersList);
  }

  static async store(req, res) {
    const connection = DB.connection();
    const transact = await connection.transaction();

    const {
      name,
      description,
    } = req.body;
    try {
      await CtlTiers.create({
        name,
        description,
      }, {
        transaction: transact,
      });
      await transact.commit();
      return res.status(HttpCode.HTTP_CREATED).json({
        message: 'Creado satisfactoriamente.',
      });
    } catch (error) {
      await transact.rollback();
      throw error;
    }
  }
}
