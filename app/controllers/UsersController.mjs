import bcrypt from 'bcryptjs';
import HttpCode from '../../configs/httpCode.mjs';
import DB from '../nucleo/DB.mjs';
import NotFoundException from '../../handlers/NotFoundExeption.mjs';

import {
  users,
  Role,
} from '../models/index.mjs';

export default class UsersController {
  static async index(req, res) {
    const usersList = await users.findAll();
    return res.status(HttpCode.HTTP_OK).json(usersList);
  }

  static async store(req, res) {
    const connection = DB.connection();
    const transact = await connection.transaction({
      autocommit: false,
    });
    const {
      name,
      password,
      email,
      code,
      idRol,
    } = req.body;
    const salt = bcrypt.genSaltSync();
    const passwordCrypt = bcrypt.hashSync(password, salt);

    try {
      const Rol = await Role.findOne({
        where: {
          id: idRol,
        },
      });
      const verifyEMail = await users.findOne({
        where: {
          email,
        },
      });
      if (verifyEMail) return res.status(HttpCode.HTTP_FORBIDDEN).json({ message: 'El email ya se encuentra registrado' });
      if (!Rol) throw new NotFoundException('NOT FOUND', 404, `No se econtro el rol con id ${idRol}`);

      const newUsr = await users.create({
        email,
        password: passwordCrypt,
        code,
        name,
        role_id: idRol,
      }, {
        transaction: transact,
      });
      await transact.commit();

      const created = await users.findOne({
        where: {
          id: newUsr.dataValues.id,
        },
      });

      return res.status(HttpCode.HTTP_CREATED).json({
        ...created.dataValues,
      });
    } catch (error) {
      await transact.rollback();
      throw error;
    }
  }

  static async findByEmail(req, res) {
    const { email } = req.body;
    const user = await users.findOne({
      where: {
        email,
      },
    });
    if (!user) throw new NotFoundException('NOT FOUND', 404, 'No se encontró el usuario');
    return res.status(HttpCode.HTTP_OK).json(user);
  }
}
