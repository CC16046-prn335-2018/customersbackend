/* eslint-disable camelcase */
import psql, {
  DataTypes,
} from 'sequelize';
import DB from '../nucleo/DB.mjs';

class CtlTiers extends psql.Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate() {
    // define association here
  }
}
CtlTiers.init({
  name: DataTypes.STRING,
  description: DataTypes.STRING
}, {
  sequelize: DB.connection(),
  modelName: 'ctl_tiers',
  timestamps: false,
});

export default CtlTiers;
