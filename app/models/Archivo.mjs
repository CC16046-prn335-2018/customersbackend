// eslint-disable-next-line import/no-cycle
import psql from 'sequelize';
import DB from '../nucleo/DB.mjs';

class Archivo extends psql.Model {
  static associate() {}
}

Archivo.init(
  {
    id: {
      type: psql.Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_tipo_archivo: {
      type: psql.Sequelize.INTEGER,
      allowNull: false,
    },
    ruta: {
      type: psql.Sequelize.STRING,
      allowNull: false,
    },
    id_relacion: {
      type: psql.Sequelize.INTEGER,
      allowNull: false,
    },
    tipo_modelo: {
      type: psql.Sequelize.STRING,
      allowNull: false,
    },
  },
  {
    timestamps: false,
    sequelize: DB.connection(),
    tableName: 'promo_archivo',
  },
);

export default Archivo;
