/* eslint-disable camelcase */
import psql, {
  DataTypes,
} from 'sequelize';
import DB from '../nucleo/DB.mjs';

class CtlStatus extends psql.Model {
  static associate() {
    // define association here
  }
}
CtlStatus.init({
  name: DataTypes.STRING,
  description: DataTypes.STRING,
}, {
  sequelize: DB.connection(),
  modelName: 'ctl_status',
  timestamps: false,
});

export default CtlStatus;
