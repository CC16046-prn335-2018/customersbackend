/* eslint-disable import/no-cycle */
/* eslint-disable camelcase */
import psql, {
  DataTypes,
} from 'sequelize';
import DB from '../nucleo/DB.mjs';
import cases from './cases.mjs';
import CombinedCaseCases from './combined_case_cases.mjs';

class CombinedCase extends psql.Model {
  static associate() {
    this.belongsToMany(cases, {
      through: CombinedCaseCases,
      foreignKey: 'combined_case_id',
      otherKey: 'case_id',
    });
  }
}
CombinedCase.init({
  name: DataTypes.STRING,
  createdAt: {
    type: psql.Sequelize.DATE,
    allowNull: false,
    defaultValue: DataTypes.NOW,
  },
}, {
  sequelize: DB.connection(),
  modelName: 'combined_case',
  timestamps: false,
});

export default CombinedCase;
