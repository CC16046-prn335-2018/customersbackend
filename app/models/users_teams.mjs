import psql, {
  DataTypes,
} from 'sequelize';
import DB from '../nucleo/DB.mjs';

class UsersTeams extends psql.Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate() {
    // define association here
  }
}
UsersTeams.init({
  team_id: DataTypes.INTEGER,
  user_id: DataTypes.INTEGER,
}, {
  sequelize: DB.connection(),
  modelName: 'users_teams',
});

export default UsersTeams;
