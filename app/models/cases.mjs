/* eslint-disable camelcase */
import psql, { DataTypes } from 'sequelize';
import DB from '../nucleo/DB.mjs';
import ctl_status from './ctl_status.mjs';
import ctl_tiers from './ctl_tiers.mjs';
import teams from './teams.mjs';
import users from './Usuario.mjs';

class cases extends psql.Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate() {
    this.belongsTo(ctl_status, {
      foreignKey: 'current_status_id',
    });
    this.belongsTo(teams, {
      foreignKey: 'team_id',
    });
    this.belongsTo(users, {
      foreignKey: 'user_id',
    });
    this.belongsTo(ctl_tiers, {
      foreignKey: 'tier_id',
    });
  }

  toJSON() {
    return {
      id: this.id,
      track_code: this.track_code,
      name: this.name,
      description: this.description,
      current_status: this.ctl_status?.name,
      current_status_id: this.current_status_id,
      team: this.team?.name,
      team_id: this.team_id,
      user: this.Usuario?.name,
      tier: this.ctl_tier?.name,
      tier_id: this.tier_id,
    };
  }
}
cases.init({
  track_code: DataTypes.STRING,
  name: DataTypes.STRING,
  description: DataTypes.STRING,
  current_status_id: DataTypes.INTEGER,
  team_id: DataTypes.INTEGER,
  user_id: DataTypes.INTEGER,
  tier_id: DataTypes.INTEGER,
}, {
  sequelize: DB.connection(),
  modelName: 'cases',
  timestamps: false,
});

export default cases;
