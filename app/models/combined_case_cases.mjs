/* eslint-disable camelcase */
import psql, {
  DataTypes,
} from 'sequelize';
import DB from '../nucleo/DB.mjs';

class CombinedCaseCases extends psql.Model {
  static associate() { }
}
CombinedCaseCases.init({
  combined_case_id: DataTypes.INTEGER,
  case_id: DataTypes.INTEGER,
}, {
  sequelize: DB.connection(),
  modelName: 'combined_case_cases',
  timestamps: false,
});

export default CombinedCaseCases;
