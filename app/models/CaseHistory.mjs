/* eslint-disable camelcase */
import psql, {
  DataTypes,
} from 'sequelize';
import DB from '../nucleo/DB.mjs';

class CaseHistory extends psql.Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate() {
    // define association here
  }
}
CaseHistory.init({
  status_id: DataTypes.INTEGER,
  case_id: DataTypes.INTEGER,
}, {
  sequelize: DB.connection(),
  modelName: 'case_history',
});

export default CaseHistory;
