/* eslint-disable camelcase */
import psql, {
  DataTypes,
} from 'sequelize';
import DB from '../nucleo/DB.mjs';

class MergedCaseCases extends psql.Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate() {
    // define association here
  }
}
MergedCaseCases.init({
  case_id: DataTypes.INTEGER,
}, {
  sequelize: DB.connection(),
  modelName: 'merged_case_cases',
});

export default MergedCaseCases;
