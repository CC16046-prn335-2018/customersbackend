import psql, {
  DataTypes
} from 'sequelize';
import DB from '../nucleo/DB.mjs';

class teams extends psql.Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate() {
    // define association here
  }
}
teams.init({
  name: DataTypes.STRING
}, {
  sequelize: DB.connection(),
  modelName: 'teams',
  timestamps: false,
});

export default teams;
