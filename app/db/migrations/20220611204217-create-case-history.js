module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('case_histories', {
      status_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'ctl_statuses',
          key: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      },
      case_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'cases',
          key: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      },
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable('case_histories');
  },
};
