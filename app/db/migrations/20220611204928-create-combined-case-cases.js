module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('combined_case_cases', {
      combined_case_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'combined_cases',
          key: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      },
      case_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'cases',
          key: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      },
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable('combined_case_cases');
  },
};
