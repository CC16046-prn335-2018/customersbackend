import moment from 'moment-timezone';
import HttpCode from '../../configs/httpCode.mjs';
import UnprocessableEntityException from '../../handlers/UnprocessableEntityException.mjs';

export default class VerifyDate {
  static isValid(date) {
    const fecha = moment(date, ['YYYY-MM-DD', 'YYYY-MM-DD HH:mm:ss'], true).isValid();
    if (!fecha) {
      throw new UnprocessableEntityException(
        'UNPROCESSABLE_ENTITY',
        HttpCode.HTTP_UNPROCESSABLE_ENTITY,
        'Fecha no valida (Format: (YYYY-MM-DD) or (YYYY-MM-DD HH:mm:ss))',
      );
    }
    VerifyDate.actual(date); /** Compara que la fecha no sea mayor a la actual */
  }

  static mayor(inicio, fin) {
    if (inicio > fin) {
      throw new UnprocessableEntityException(
        'UNPROCESSABLE_ENTITY',
        HttpCode.HTTP_UNPROCESSABLE_ENTITY,
        'La fecha inicio no puede ser mayor que la fecha fin',
      );
    }
  }

  static actual(fecha) {
    const ahora = moment().format('YYYY-MM-DD');
    if (fecha > ahora) {
      throw new UnprocessableEntityException(
        'UNPROCESSABLE_ENTITY',
        HttpCode.HTTP_UNPROCESSABLE_ENTITY,
        'La fecha no puede ser mayor que la actual',
      );
    }
  }
}
