import HttpCode from '../../configs/httpCode.mjs';
import BadRequestException from '../../handlers/BadRequestException.mjs';
import { Usuario } from '../models/index.mjs';

export default class validateEmail {
  static async exist(email) {
    const user = await Usuario.findOne({ where: { email } });
    if (user) {
      throw new BadRequestException('ingrese un email diferente', HttpCode.HTTP_BAD_REQUEST, 'email ya existe');
    }
  }
}
