export default class ValidateDocument {
  static isNIT(nit) {
    if (nit === null) return false;

    let valido = false;

    // Validando la longitud del numero de DUI
    if (nit.length === 17) {
      // Validando que no sea ceros todos los dígitos
      if (
        nit !== '0000-000000-000-0'
                && nit.match(/^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]$/)
      ) {
        // Obteniendo los dígitos y el verificador por separado
        // eslint-disable-next-line prefer-const
        let [ubicacion, fecha, correlativo, validador] = nit.split('-');
        const digitos = (ubicacion + fecha + correlativo).split('');

        // Convirtiendo los datos a tipo integer
        validador = parseInt(validador, 10);
        correlativo = parseInt(correlativo, 10);
        let suma = 0;
        let mod = 0;
        if (correlativo <= 100) {
          // Obteniendo la suma corresponiente

          // eslint-disable-next-line no-return-assign,no-param-reassign
          suma = digitos.reduce((total, digito, index) => (total += parseInt(digito, 10) * (14 - index)), 0);
          // Obteniendo el Modulo base 11
          mod = suma % 11;
          mod = mod === 10 ? 0 : mod;
        } else {
          // Obteniendo la suma corresponiente
          // eslint-disable-next-line no-return-assign,no-param-reassign
          suma = digitos.reduce((total, digito, index) => (total += parseInt(digito, 10) * parseInt(3 + 6 * Math.floor((index + 5) / 6) - (index + 1), 10)), 0);
          // Obteniendo el Modulo base 11
          mod = suma % 11;
          mod = mod > 1 ? 11 - mod : 0;
        }
        if (mod === validador) {
          valido = true;
        }
      }
    }
    return valido;
  }

  static isDUI(dui) {
    if (dui === null) return false;
    let valido = false;

    // Validando la longitud del numero de DUI
    if (dui.length === 10) {
      // Validando que no sea ceros todos los dígitos
      if (dui !== '00000000-0') {
        // Obteniendo los dígitos y el verificador
        let [digitos, validador] = dui.split('-');

        // Verficiando que la cadena
        if (
          typeof digitos !== 'undefined'
                    && typeof validador !== 'undefined'
        ) {
          // Verificando que el validador sea de un solo caracter
          if (validador.length === 1) {
            // Convirtiendo los digitos a array
            digitos = digitos.split('');

            // Convirtiendo los datos a tipo integer
            validador = parseInt(validador, 10);
            digitos = digitos.map((digito) => parseInt(digito, 10));

            // Obteniendo la suma corresponiente
            // eslint-disable-next-line no-return-assign,no-param-reassign
            const suma = digitos.reduce((total, digito, index) => (total += digito * (9 - index)), 0);

            // Obteniendo el Modulo base 10
            let mod = suma % 10;
            mod = validador === 0 && mod === 0 ? 10 : mod;

            const resta = 10 - mod;

            if (resta === validador) {
              valido = true;
            }
          }
        }
      }
    }
    return valido;
  }

  static isEmail(email) {
    let valido = false;
    // eslint-disable-next-line no-useless-escape
    const regexPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // eslint-disable-next-line camelcase
    if (regexPattern.test(email)) {
      valido = true;
    }
    return valido;
  }
}
