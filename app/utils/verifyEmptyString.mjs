import HttpCode from '../../configs/httpCode.mjs';
import BadRequestException from '../../handlers/BadRequestException.mjs';

export default class VerifyEmptyString {
  static emptyString(strg, message = 'Campo no valido') {
    const isEmpty = (str) => !str || !str.trim();
    if (isEmpty(strg)) {
      throw new BadRequestException('BAD_REQUEST', HttpCode.HTTP_BAD_REQUEST, message);
    }
  }
}
