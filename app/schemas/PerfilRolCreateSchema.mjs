const perfilRolCreateSchema = {
  type: 'object',
  properties: {
    id_perfil: {
      type: 'number',
    },
    id_rol: {
      type: 'number',
    },
  },
};

export default perfilRolCreateSchema;
