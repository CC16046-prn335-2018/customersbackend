import { Router } from 'express';
import CasesController from '../../app/controllers/CasesController.mjs';
import Call from '../../app/utils/Call.mjs';

const router = Router();
router.get('/', Call(CasesController.index));
router.post('/', Call(CasesController.store));
router.put('/close', Call(CasesController.closeCase));
router.put('/escale', Call(CasesController.escaleCase));
router.get('/combine', Call(CasesController.getCasesToCombine));
router.post('/combine', Call(CasesController.combineCases));
router.get('/combined/list', Call(CasesController.getCombinedCases));
export default router;
