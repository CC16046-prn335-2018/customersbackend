import { Router } from 'express';
import TiersController from '../../app/controllers/TiersController.mjs';
import Call from '../../app/utils/Call.mjs';

const router = Router();
router.get('/', Call(TiersController.index));
router.post('/', [], Call(TiersController.store));
export default router;
