import { Router } from 'express';
import StatusController from '../../app/controllers/StatusController.mjs';
import Call from '../../app/utils/Call.mjs';

const router = Router();
router.get('/', Call(StatusController.index));
export default router;
