import { Router } from 'express';
import TeamsController from '../../app/controllers/TeamsController.mjs';
import Call from '../../app/utils/Call.mjs';

const router = Router();
router.get('/', Call(TeamsController.index));
router.post('/', [], Call(TeamsController.store));
export default router;
